[TypeScript com NodeJS do jeito certo!](https://www.youtube.com/watch?v=aTf8QTjw4RE)

# Configurando o Eslint

\$ yarn eslint --init

? How would you like to use ESLint? To check syntax, find problems, and enforce code style
? What type of modules does your project use? JavaScript modules (import/export)
? Which framework does your project use? None of these
? Does your project use TypeScript? Yes
? Where does your code run? Browser
? How would you like to define a style for your project? Use a popular style guide
? Which style guide do you want to follow? Standard: https://github.com/standard/standard
? What format do you want your config file to be in? JavaScript

? Would you like to install them now with npm? Yes
Despois de responder essa pergunta ele faz a instalação com o npm. Deletar o package-lock.json e rodar "\$ yarn" para registrar as dependencias no yarn.lock

Guias de estilo disponíveis:
Airbnb: https://github.com/airbnb/javascript
Standard: https://github.com/standard/standard
Google: https://github.com/google/eslint-config-google

## editar as Settings do vscode

Incluir:

```json
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact"
    ]
```

# Instalando o Prettier

\$ yarn add prettier eslint-config-prettier eslint-plugin-prettier -D

## Configuração no vsCode

Incluir no settings.json:

```json
    "editor.formatOnSave": true,
    "[typescript]": {
        "editor.formatOnSave": false
    },
    "[typescriptreact]": {
        "editor.formatOnSave": false
    },
```

## Container MongoDB

```
$ docker run \
    --name mongodb \
    -p 27017:27017 \
    -d mongo
```
